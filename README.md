# Ifetch

A system information fetcher that use inxi

# Dependencies

- Bash
- whoami (part of GNU Coreutils)
- inxi

# Running Ifetch

Download the bash script and run it.

# Screenshot

https://gitlab.com/Meztli_RA/ifetch/-/blob/master/Ifetch%20Screenshot.png